﻿CREATE TABLE [dbo].[Users] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (MAX) NULL,
    [Email]    NVARCHAR (MAX) NULL,
    [Password] NVARCHAR (MAX) NULL,
    [Birthday] DATETIME       NOT NULL,
    [Gender]   BIT            NOT NULL,
    CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED ([Id] ASC)
);

