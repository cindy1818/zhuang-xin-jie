﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    [MetadataType(typeof(UsersMeteData))]
    public partial class Users
    {

    }
    public class UsersMeteData
    {
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("姓名")]
        [StringLength(10)]
        public int Id { get; set; }
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("姓名")]
        [StringLength(10)]
        public string Name { get; set; }
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("姓名")]
        [StringLength(10)]
        public string Email { get; set; }
        public string Password { get; set; }
        public System.DateTime Birthday { get; set; }
        public bool Gender { get; set; }
    }
}