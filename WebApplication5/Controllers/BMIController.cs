﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication5.ViewModels;

namespace WebApplication5.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()/*顯示view*/
        {
            return View(new Class1());
        }
        [HttpPost]
                                //float h, float w
                                //類別 物件
                                //取裡的變數class
        public ActionResult Index(Class1 data)/*多載:建立數項名稱相同但輸入輸出類型或個數不同的子程式，所以必需獨立出來*/
        {
            //if (data.h<50 ||data.h>200)
            //{
            //    ViewBag.HeightError = "身高請輸入50~200的數值";

            //if (data.w < 50 || data.w > 200)
            //{
            //    ViewBag.HeightError = "身高請輸入50~200的數值";
            //}
            //float m_h = h / 100;
            //float bmi = w / (m_h * m_h);
            if (ModelState.IsValid)
            { 
                float m_h = data.h / 100;
                float bmi = data.w / (m_h * m_h);

            
                string level = "";
                if (bmi < 18.5)
                {
                    level = "太瘦";
                }
                else if (bmi > 18.5 && bmi < 24)
                {
                     level="適中";
                }
                else if (bmi>35)
                {
                    level = "太胖";
                }

                //ViewBag.BMI = bmi;
                //ViewBag.level = level;

                data.BMI = bmi;
                data.level = level;
            }
            return View(data);
        }
    }
}   